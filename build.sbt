name := "persist-sandbox"

version := "1.0"

scalaVersion := "2.10.2"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3-M2",
  "com.typesafe.akka" %% "akka-persistence-experimental" % "2.3-M2",
  "com.typesafe.akka" %% "akka-testkit" % "2.3-M2",
  "org.specs2" %% "specs2" % "2.3.7"      
)