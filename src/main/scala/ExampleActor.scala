import akka.actor.ActorLogging
import akka.persistence.EventsourcedProcessor

class ExampleActor extends EventsourcedProcessor with ActorLogging {

  def receiveReplay: Receive = {
    case ev =>
      log.info("received event")
  }

  def receiveCommand: Receive = {
    case ev =>
      log.info("received command")
  }
}
