import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestActorRef, TestActor}
import org.specs2.mutable.{After, Specification}

class ExampleActorSpecs extends Specification {

  trait mocks extends After {
    implicit val system = ActorSystem("test")
    val ref = TestActorRef[ExampleActor]
    val actor = ref.underlyingActor
    override def after {
      system.shutdown()
    }
  }

  "example actor" should {
    "do something" in new mocks {
      1 mustEqual 1
    }
  }

}
